<?php 
 
//Number1 
    function fileLine($fileName, $lineNumber){
        if(file_exists($fileName)){
            $file=file($fileName);
            echo $file[$lineNumber-1]."<br>";
        }else{
            echo "File does not exist<br>";
        }
    }
fileLine("sample.txt", 2);

   //Number2
function appendLine($fileName, $string, $lineNumber){
    $file=fopen($fileName,'a+'); 
    $file2=file($fileName,FILE_IGNORE_NEW_LINES);
    array_splice($file2,$lineNumber-1,0,$string);
    file_put_contents($fileName, join("\n", ($file2)));
    echo "Appended to $fileName <br>";
    fclose($file);
}
appendLine('sample.txt','Hello World', 5);   

//Number3
function randomLiner($fileName){
    // echo $fileName;
    $file2=file($fileName);
    $file=substr($fileName,0,strpos($fileName, '.'));
    $count=0;
    if(file_exists($fileName)){
        fopen($fileName,'r');
        // echo count_chars($fileName,1);
        foreach(count_chars($file) as $letters=>$val){
            switch(strtolower(chr($letters))){
                case 'a':
                    $count+=$val;
                    break;
                case 'e':
                    $count+=$val;
                    break;
                case 'i':
                    $count+=$val;
                    break;
                case 'o':
                    $count+=$val;
                    break;
                case 'u':
                    $count+=$val;
                    break;
                default:
                    $count+=0;
                    break;

            }
        }
        
        if($count%2==1){
            echo $file2[5-1]."<br>";
           
        }else{
            echo $file2[2-1]."<br>";
        }
        // echo $count;
        

    }else{
        echo "No file exists";
    }
};
randomLiner('sample.txt');
//Number4
function testLoyalty($name){
    $name=strtolower($name);
    $count=0;
    // echo count_chars($name);
    foreach(count_chars($name,1) as $letter=>$val){
        // echo count_chars($name);

        if(chr($letter)=='e'){
            $count+=$val;
        }
        if(chr($letter)=='a'){
            $count+=$val;
        }
        if(chr($letter)=='n'){
            $count+=$val;
        }
        // echo "There were $val instance(s) of \"" , chr($letter) , "\" in the string.\n";
    }
    if($count>=3 && ($count*strlen($name))%6==0){
        echo "Loyal<br>";
    }else{
        echo "Di Sure<br>";
    }
}
testLoyalty('Victoriano');  
testLoyalty('Eleassarr');

//Number5
function calculateFactorial($number){
    $factorial=0;
    $tempNum=$number;
    if($number>=0){
        while($tempNum>0){
            $num=$tempNum-1;
            $factorial+=$tempNum*$num;
            $tempNum-=1;
        }
        echo "The factorial of $number is $factorial<br>";
        
    }else{
        echo abs($number);
    }
}
calculateFactorial(5);

//Number6
function checkPrimeNumber($number){
    for ($i = 2; $i <= $number/2; $i++){
        if ($number % $i == 0)
            return "This is not a prime number<br>";
    }
    return  "This is a prime number<br>";
        
}
echo checkPrimeNumber(2);

//Number7
function reverseHalf($string){
    $count=(int)strlen($string);
    if($count%2==0){
        echo strrev(substr($string,($count/2)))."<br>";
    }else{
        echo strrev($string)."<br>";
    }
}
reverseHalf("Paolul");
reverseHalf("Eleasar");
?>